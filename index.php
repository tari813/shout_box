<?php include 'database.php'; ?>
<?php

    $query = 'SELECT * FROM shouts ORDER BY id DESC';
    $results = mysqli_query($con, $query);
?>

<!doctype html>
<html>
<head>
    <title>Shout Out Box</title>
    
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    
    <meta charset="utf-8" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="style.css" type="text/stylesheet">
</head>

<body>
    
    <div class="container">
        <div class="shoutit">
            <h1>Shout Box!!</h1>
            <div class="shout-window">
                <ul>
                    <?php while($row = mysqli_fetch_assoc($results)) : ?>
                    <li class="shouts"><span><?php echo $row['time']; ?> - </span><?php echo $row['user']; ?>: <?php echo $row['message']; ?></li>
                    <?php endwhile; ?>
                </ul>
            </div>
            <form method="post" action="process.php">
                <?php if(isset($_GET['error'])) : ?>
                    <div class="btn-danger"><?php echo $_GET['error']; ?></div>
                <?php endif; ?>
                <input type="text" name="user" placeholder="Enter your name" />
                <input type="text" name="message" placeholder="Enter your message to the world" />
                <input class="btn btn-primary btn-lg" type="submit" name="submit" value="Shout Out" />
                <div></div>
            </form>
        </div>
    </div>

</body>
</html>

